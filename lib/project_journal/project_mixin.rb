# Copyright (C) 2021-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module ProjectJournal
  module ProjectMixin
    class << self
      def prepended(base)
        base.has_many :journals,
                      :as => :journalized,
                      :dependent =>:destroy,
                      :inverse_of => :journalized
        base.after_save :create_journal
      end
    end

    def init_journal(user)
      @current_journal ||= Journal.new(:journalized => self, :user => user)
    end

    def create_journal
      if @current_journal
        # TODO: Need Redmine improvement to enable notification
        @current_journal.notify = false
        @current_journal.save
      end
    end

    def journalized_attribute_names
      Project.column_names - [
        "id",
        "created_on",
        "updated_on",
        "lft",
        "rgt",
      ]
    end

    def set_watcher(user, watching=true)
    end

    def notified_watchers
      []
    end

    def visible_journals_with_index(user=User.current)
      return [] unless user.allowed_to?(:edit_project, self)

      visible_journals = journals.
                           preload(:details).
                           preload(:user => :email_address).
                           reorder(:created_on, :id).to_a

      visible_journals.each_with_index {|j,i| j.indice = i+1}

      Journal.preload_journals_details_custom_fields(visible_journals)
      visible_journals.select! do |journal|
        journal.notes? || journal.visible_details.any?
      end
      visible_journals.reverse! if user.wants_comments_in_reverse_order?
      visible_journals
    end
  end
end

Project.prepend(ProjectJournal::ProjectMixin)
